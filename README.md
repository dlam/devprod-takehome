# Introduction

Thank you for taking the time to participate in this coding exercise.  This exercise 
represents a real life tool that was needed.  The purpose of this assignment is to 
give you the oppertunity to demonstrate how you would solve this problem.  We will
be grading your assignment as if a peer was performing a code review.  We will be 
looking at all aspects of the work you submit, from implementation, design, documentation
and test considerations.  

# Challenge

Write a job that branches from the main development line every other Friday at noon.
The new branch name should be of the form `release-<version_number>`.  After branching,
increment the minor version number of the main application.

The version number can be found in the build.gradle file, currently numbered `1.3.0`.  
In this example, the new release branch should be `release-1_3_0` and the main
development line should have it's version increased to `1.4.0`.

Your task is to configure the job as well as set a schedule so it executes at the 
appropriate time.

Please fork this repositry and complete the work in your own repo.  Please put 
together a Merge Request with all the code changes, and document accordingly.

All information below is provided for reference only. If you have any questions,
please don't hesitate to email.

## What's contained in this project

### Android code

The state of this project is as if you followed the first few steps in the linked
[Android tutorial](https://developer.android.com/training/basics/firstapp/) and
have created your project. You're definitely going to want to open up the
project and change the settings to match what you plan to build. In particular,
you're at least going to want to change the following:

- Application Name: "My First App"
- Company Domain: "example.com"

### Fastlane files

It also has fastlane setup per our [blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) on
getting GitLab CI set up with fastlane. Note that you may want to update your
fastlane bundle to the latest version; if a newer version is available, the pipeline
job output will tell you.

### Dockerfile build environment

In the root there is a Dockerfile which defines a build environment which will be
used to ensure consistent and reliable builds of your Android application using
the correct Android SDK and other details you expect. Feel free to add any
build-time tools or whatever else you need here.

We generate this environment as needed because installing the Android SDK
for every pipeline run would be very slow.

### Gradle configuration

The gradle configuration is exactly as output by Android Studio except for the
version name being updated to 

`versionName "1.3.0"`

### Build configuration (`.gitlab-ci.yml`)

The sample project also contains a basic `.gitlab-ci.yml` which will successfully 
build the Android application.


# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Blog post: Android publishing with GitLab and fastlane](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/)
